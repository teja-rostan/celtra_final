"""
 The definition of stream (video flow) objects.

 @author: Teja Roštan
"""


class VideoFlow:

    def __init__(self, name, alpha, beta, quality):
        """
        The video flow is one of the streams that takes only one piece of a 360 degrees video, 
        that starts at the angle alpha and ends at the angle beta and has a video quality in [1..100]
        :param name: the name of the video stream
        :param alpha: the first angle of the stream (counter clock wise)
        :param beta:  the last angle of the stream (counter clock wise)
        :param quality: the quality of the video stream
        """
        self.name = name
        self.alpha = alpha
        self.beta = beta - 360 * (beta > 359)
        self.quality = quality

    def get_name(self):
        return self.name

    def get_medium(self):
        beta = self.beta
        if self.beta < self.alpha:
            beta += 360
        medium = (beta - self.alpha) / 2 + self.alpha
        medium -= 360 * (medium > 359)
        return medium

    def get_solution(self):
        return self.name+','+str(self.alpha)+','+str(self.beta)+','+str(self.quality)+'\n'

    def get_info(self):
        print("i'm " + self.name + " with a="+str(self.alpha)+", b="+str(self.beta)+" and q="+str(self.quality))

    def set_quality(self, quality):
        self.quality = quality

