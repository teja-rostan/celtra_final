"""
 The program takes the "results_angles.csv" (the predicted angles at time-points) and calculates
 the streams to preload at the time-points in the Celtras' defined format solution in 
 file "soluton_29_5.csv".  

 @author: Teja Roštan (player 4)
"""
import pandas as pd
import VideoFlow as vf
import numpy as np


def main():
    data = pd.read_csv("my_data_31_5.csv")
    angles = data.as_matrix()[:, 1]
    streams = create_streams()
    chosen_streams = choose_streams(angles, streams)
    f = open("solution_31_5.csv", 'w')
    f.write('')
    f.close()

    f = open("solution_31_5.csv", 'a')
    for stream in streams:
        f.write(stream.get_solution())
    for i, streams_list in enumerate(chosen_streams):
        line = ''
        for stream in streams_list:
            line +=stream.get_name()+','
        f.write('\n'+line[:-1])
    f.close()


def choose_streams(angles, streams):
    chosen_streams = list()
    mediums = get_all_mediums(streams)[1:]
    for a in angles:
        distances = np.min(np.vstack([np.abs(a-mediums), np.abs(a+360-mediums), np.abs(a+mediums+360)]).T, axis=1)
        nearest = np.argmin(distances)
        list_of_streams = [streams[0], streams[nearest+1]]
        chosen_streams.append(list_of_streams)
    return chosen_streams


def choose_streams_v2(angles, streams):
    chosen_streams = list()
    mediums = get_all_mediums(streams)[1:91:3]
    for a in angles:
        distances = np.min(np.vstack([np.abs(a-mediums), np.abs(a+360-mediums), np.abs(a+mediums+360)]).T, axis=1)
        nearest = np.argmin(distances)*3
        list_of_streams = [streams[0], streams[nearest+1], streams[nearest+2], streams[nearest+3]]
        print(a)
        streams[nearest + 1].get_info()
        streams[nearest + 2].get_info()
        streams[nearest + 3].get_info()
        chosen_streams.append(list_of_streams)
    return chosen_streams


def create_streams():
    streams = list()
    streams.append(vf.VideoFlow(name="S0", alpha=0, beta=359, quality=1))
    for i, alpha in enumerate(np.arange(start=0, step=4, stop=359)):
        streams.append(vf.VideoFlow(name="S"+str(i+1), alpha=alpha, beta=alpha+95, quality=100))
    return np.asarray(streams)


def create_streams_v2():
    streams = list()
    streams.append(vf.VideoFlow(name="S0", alpha=0, beta=359, quality=1))
    idx = 1
    for alpha in np.arange(start=0, step=12, stop=359):
        streams.append(vf.VideoFlow(name="S"+str(idx), alpha=alpha, beta=alpha+71, quality=100))
        alpha_before = alpha - 36
        alpha_before += 360 * (alpha_before < 0)
        alpha_after = alpha + 72
        alpha_after -= 360 * (alpha_after > 359)
        beta_after = alpha + 72 + 35
        beta_after -= 360 * (beta_after > 359)
        beta_before = alpha - 1
        beta_before += 360 * (beta_before < 0)
        streams.append(vf.VideoFlow(name="S"+str(idx+1), alpha=alpha_before, beta=beta_before, quality=33))
        streams.append(vf.VideoFlow(name="S"+str(idx+2), alpha=alpha_after, beta=beta_after, quality=33))
        idx += 3
    return np.asarray(streams)


def get_all_mediums(streams):
    mediums = np.empty(streams.shape)
    for i, s in enumerate(streams):
        mediums[i] = s.get_medium()
    return mediums


if __name__ == '__main__':
    main()